## SQL - Exercice ##

1. Créer une base de données nommée compagnieDB
2. Créer les tables avions, pilotes et vols

Table avions
NA : numéro avion de type entier,
Nom : nom avion de type texte (12),
Capacite : capacité avion de type entier,
Localite : ville de localité de l’avion de type texte (10)

Table pilotes
NP : numéro pilote de type entier,
Nom : nom du pilote de type texte (25),
Adresse : adresse du pilote de type texte (40)

Table vols
NV : numéro de vol de type texte (6),
VD : ville de départ de type texte (10),
VA : ville d’arrivée de type texte (10),
NP : numéro de pilote de type entier,
NA : numéro avion de type entier

3. Insérer les avions suivants dans la table avions :
(100, AIRBUS, 300, BERLIN), (101, B737, 250, LONDRES), (102, AIRBUS, 220, BERLIN), (103, BOEING, 500, PARIS)

Insérer les pilotes suivants dans la table pilotes :
(121, "Pierre", "pierre@gmail.com"), (343, "Paul", "paul@gmail.com"), (222, "Etienne", "etienne@gmail.com")

Insérer les vols suivants dans la table vols :
("IT100", 121, 103, "Berlin", "Paris"), ("IT102", 222, 102, "Londres", "Berlin"), ("IT103", 343, 100, "Paris", "Berlin"), ("IT104", 343, 101, "Paris", "Londres")

2. Afficher tous les avions
3. Afficher tous les avions par nom et par ordre croissant
4. Afficher les noms et les capacités des avions
5. Afficher les localités des avions sans redondance
6. Afficher les avions dont la localité est Londres ou Berlin
7. Modifier la capacité de l’avion numéro 101, la nouvelle capacité est 220
8. Supprimer les avions dont la capacité est inférieure à 200
9. Afficher la capacité maximale, minimale, moyenne des avions
10. Afficher les données des avions dont la capacité est la plus basse
11. Afficher les données des avions dont la capacité est supérieure à la capacité moyenne
12. Afficher le nom et l’adresse des pilotes assurant les vols IT100 et IT104
13. Afficher les numéros des pilotes qui sont en service
14. Afficher les numéros des pilotes qui ne sont pas en service
15. Afficher les noms des pilotes qui conduisent un AIRBUS